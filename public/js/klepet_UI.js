function divElementEnostavniTekstKanal(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold; background-color: #FFFFCC"></div>').html(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

var img = ['<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"/>', 
'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"/>',
'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"/>',
'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"/>',
'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"/>'];
var oz = [/\;\)/g, /\:\)/g, /\:\*/g, /\:\(/g, /\(y\)/g];

//xss napad
var odstraniVulgarneBesede = function(sporocilo){
  for(var i = 0; i < besede.length; i++){
    var tmp = '';
    for(var j = 0; j < besede[i].length; j++){
      tmp = tmp.concat('*');
    }
    var re = "\\b" + besede[i] + "\\b";
    sporocilo = sporocilo.replace(new RegExp( re, "gi"), tmp);
  }
  return sporocilo;
}

function vrniImeKanala(){
  var ime = $('#kanal').text();
  var index = ime.indexOf("@");
  return ime.substring(index + 2, ime.length);
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;
  
  sporocilo = sporocilo.replace('<','&lt');
  sporocilo = sporocilo.replace('>','&gt');

  for(var i = 0; i < img.length; i++){
      sporocilo = sporocilo.replace(oz[i],img[i]);
  }

  sporocilo = odstraniVulgarneBesede(sporocilo);

  $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    var ime = vrniImeKanala();
    klepetApp.posljiSporocilo(ime, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();  //sprememba
var besede;

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  besede = $('<div>').load("../swearWords.txt", function() {
    besede = besede.text().split("\n");
  });

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      var ime = $('#kanal').text();
      ime = ime.split(" ");
      var imeKanala = ime[ime.length - 1];
      $('#kanal').text(rezultat.vzdevek + " @ " + imeKanala );
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.vzdevek + ' @ ' + rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  socket.on('uporabniki', function (u) {
    $('#seznam-uporabnikov').empty();
    console.log(u);
    for(var i = 0; i < u.length; i++){
      $('#seznam-uporabnikov').append(divElementEnostavniTekstKanal(u[i]));
    }
  });
  
  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekstKanal(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', vrniImeKanala());
  }, 1000); 
 
  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});