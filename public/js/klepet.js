var Klepet = function(socket) {
  this.socket = socket;
};
//komentar
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.posljiZasebnoSporocilo = function(uporabnik, besedilo) {
  var sporocilo = {
    uporabnik: uporabnik,
    besedilo: besedilo
  };
  this.socket.emit('zasebnoSporocilo', sporocilo);
};


Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.spremeniVarovaniKanal = function(imeKanala, geslo){
  this.socket.emit('pridruzitevZahtevaVarovano', {
    novKanal : imeKanala,
    geslo : geslo
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift(); 
      var tmp = besede[0];
      //console.log(tmp.substring(0,1) + "<-zacetel   konec->" + tmp.substring(tmp.length - 1, tmp.length));
      if(tmp.substring(0,1) === "\"" && tmp.substring(tmp.length - 1, tmp.length) === "\""){
        var imeKanala = tmp.substring(1, tmp.length - 1);
        besede.shift();
        var geslo = besede.join(' ');
        geslo = geslo.substring(1, geslo.length - 1);
        console.log(tmp, geslo);
        this.spremeniVarovaniKanal(imeKanala, geslo);
      }
      else{
        var kanal = besede.join(' ');
        this.spremeniKanal(kanal); 
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var uporabnik = besede[0];
      besede.shift();
      var sp = besede.join(' ');
      //console.log("uporebnik: "+ uporabnik+". besedilo: "+ sp+ "   -> zasebno")
      this.posljiZasebnoSporocilo(uporabnik, sp);
      break;
    default:        //posiljanje zasebnih sporocil
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};
