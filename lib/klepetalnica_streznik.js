var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var uporabnikiNaKanalu = [];
var gesla = [];
var varovaniKanal = [];

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajPosredovanjeZasebnegaSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    uporabniki(socket,trenutniKanal[socket.id]);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    socket.on('uporabniki', function (kanal) { 
      socket.emit('uporabniki', uporabniki(kanal)); 
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  uporabniki(socket, trenutniKanal[socket.id]);
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal,vzdevek : vzdevkiGledeNaSocket[socket.id]});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });
 
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
    
  }
}

function uporabniki(kanal){  
  var uporabniki = io.sockets.clients(kanal);
  while(uporabnikiNaKanalu.length > 0) {
    uporabnikiNaKanalu.pop();
  }
  for (var i in uporabniki) {
    var uporabnikSocketId = uporabniki[i].id;
    uporabnikiNaKanalu[i] = vzdevkiGledeNaSocket[uporabnikSocketId];
  };
  //console.log(kanal);
  return uporabnikiNaKanalu;
}

function pridruzitevVarovanemuKanalu(socket, imeKanala, geslo){
  var index = -1;
  var pridruziSe = false;
  for(var i = 0; i < varovaniKanal.length; i++){
    if(varovaniKanal[i].localeCompare(imeKanala) === 0){
      index = i;
    }
  }
  //console.log("Index: " + index);
  if(index === -1){
    var zeObstaja = false;
    var uporabnikiNaKanalu = io.sockets.clients();
    for (var i in uporabnikiNaKanalu) {
      var uSocketId = uporabnikiNaKanalu[i].id;
      //console.log(trenutniKanal[i] + "<- trenutniKanal[i]    imeKanala->" + imeKanala);
      if(trenutniKanal[uSocketId].localeCompare(imeKanala) === 0){
        io.sockets.socket(socket.id).emit('sporocilo', {
          besedilo: "Izbrani kanal " + imeKanala + " je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev " + imeKanala + " ali zahtevajte kreiranje kanala z drugim imenom."
        });
        zeObstaja = true;
        break;
      }
    }
    if(!zeObstaja){
      pridruziSe = true;
       //console.log("Ne obstaja se kanal!");
    }
  }
  else if(gesla[index].localeCompare(geslo) === 0){
     pridruziSe = true;   
  }
  else{
    io.sockets.socket(socket.id).emit('sporocilo', {
      besedilo: 'Pridružitev v kanal ' + imeKanala + ' ni bilo uspešno, ker je geslo napačno!'
    });
  }
  if(pridruziSe){
    //console.log("PridruziSe!");
    socket.leave(trenutniKanal[socket.id]);
    varovaniKanal.push(imeKanala);
    gesla.push(geslo);
    socket.join(imeKanala);
    socket.emit('pridruzitevOdgovor', {kanal: imeKanala, vzdevek : vzdevkiGledeNaSocket[socket.id]});
    socket.broadcast.to(imeKanala).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + imeKanala + '.'
    });
    
    uporabnikiNaKanalu = io.sockets.clients(imeKanala);
    if (uporabnikiNaKanalu.length > 1) {
      var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + imeKanala + ': ';
      for (var i in uporabnikiNaKanalu) {
        var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        if (uporabnikSocketId != socket.id) {
          if (i > 0) {
            uporabnikiNaKanaluPovzetek += ', ';
          }
          uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
        }
      }
      uporabnikiNaKanaluPovzetek += '.';
      socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
    }
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') === 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPosredovanjeZasebnegaSporocila(socket) {      //sporocilo.uporabnik = uporabnik do katerega hocemo dostopat; sporocilo.besedilo-> kaj mu hocemo poslati
  socket.on('zasebnoSporocilo', function (sporocilo) {        //posliji le enemu uporabniku sporocilo in to tistemu, ki ima sporocilo.uporabnik za svoj vzdevek!
                                                              //primeru, da uporabnik ne obstaja in sicer: "Sporočilo <sporočilo> uporabniku z vzdevkom <vzdevek> ni bilo mogoče posredovati.".
    var pravo = true;                                                          //to tudi ce posljemo samemu sebi!
    var klienti = io.sockets.clients();
    var obstaja = false;
    var prejemnikovId;
    var iskaniVzdevek = sporocilo.uporabnik.substring(1, sporocilo.uporabnik.length - 1);
    var sp = sporocilo.besedilo.substring(1, sporocilo.besedilo.length - 1);
    if(sporocilo.uporabnik[0] !== '"' && sporocilo.uporabnik[sporocilo.uporabnik.length - 1] !== '"' &&  sporocilo.besedilo[0] !== '"' && sporocilo.besedilo[sporocilo.besedilo.length - 1] !== '"'){
      pravo = false;
    }
    for(var k = 0; k < klienti.length; k++){
      var uporabnikSocketId = klienti[k].id;
      //console.log("vzdevkiGledeNaSocket[uporabnikSocketId ]: " + vzdevkiGledeNaSocket[uporabnikSocketId ]);
      //console.log("sporocilo.uporabnik :" + iskaniVzdevek);
      if(vzdevkiGledeNaSocket[uporabnikSocketId ] === iskaniVzdevek){
        prejemnikovId = uporabnikSocketId;
        obstaja = true;
      }
    }
    //console.log(sporocilo.uporabnik+"<-uporabnik     vzdevkiGNS[socket.id]-> "+vzdevkiGledeNaSocket[socket.id]);
    if(!pravo){
      io.sockets.socket(socket.id).emit('sporocilo', {
        besedilo: 'Sporočilo "' + sporocilo.besedilo + '" uporabniku z vzdevkom ' + sporocilo.uporabnik + " ni bilo mogoče posredovati. Napačen ukaz!"    //zdi se mi bolj pregledno če ima sporocilo o napaki narekovaje
      });
    }
    else if(iskaniVzdevek === vzdevkiGledeNaSocket[socket.id] || !obstaja){
      io.sockets.socket(socket.id).emit('sporocilo', {
        besedilo: "Sporočilo " + sporocilo.besedilo + " uporabniku z vzdevkom " + sporocilo.uporabnik + " ni bilo mogoče posredovati."    //zdi se mi bolj pregledno če ima sporocilo o napaki narekovaje
      });
    }
    else{
      io.sockets.socket(prejemnikovId).emit('sporocilo', {
        besedilo: vzdevkiGledeNaSocket[socket.id] + ' (zasebno): ' + sp     // v izpisu uporabniku brez narekovajev
      });
      io.sockets.socket(socket.id).emit('sporocilo', {
        besedilo: "(zasebno za " + iskaniVzdevek + "): " + sp   //zdi se mi bolj pregledno če ima sporocilo o napaki narekovaje
      });
    }
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    if(varovaniKanal.indexOf(kanal.novKanal) === -1) {
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal.novKanal);
      //console.log("obdelajPridruzitevKanalu");
    }
    else{
      io.sockets.socket(socket.id).emit('sporocilo', {
        besedilo: "Izbrani kanal je zaščiten!"
      });
    }
  });
  socket.on('pridruzitevZahtevaVarovano', function(kanal) {
    var imeKanala = kanal.novKanal;
    var geslo = kanal.geslo;
    //console.log("obdelajPridruzitevVarovanemuKanalu");
    pridruzitevVarovanemuKanalu(socket, imeKanala, geslo);
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}